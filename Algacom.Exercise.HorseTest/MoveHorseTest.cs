﻿using Algacom.Exercise.Horse;
using NUnit.Framework;
using System.Collections.Generic;
using System.Diagnostics;

namespace Algacom.Exercise.HorseTest
{
	[TestFixture]
	public class MoveHorseTest
	{
		[Test]
		public void Must_Valid_Move_Horse()
		{
			var rules = new RulesHorse();
			var validRules = rules.GetRules();
			Assert.That(validRules[1].Count, Is.EqualTo(2));
			Assert.That(validRules[4].Count, Is.EqualTo(3));

		}


		[Test]
		public void Must_Valid_Numbers_Build()
		{
			var rules = new RulesHorse();
			var validRules = rules.GetRules();
			var build = new BuildNumers();
			var result = build.GetValidNumbersByPivote(1, validRules);
			Assert.That(result.Count > 0);
			Assert.That(result.Count, Is.EqualTo(136));
			Assert.That(result.Contains("1818181"));
			Assert.That(result.Contains("1818183"));
	
		}

		[Test]
		public void Must_Valid_Numbers_Build_By_Pivotes()
		{
			var rules = new RulesHorse();
			var validRules = rules.GetRules();
			var build = new BuildNumers();
			var stopWhat = new Stopwatch();
			stopWhat.Start();
			var result = build.GetValidNumbersByPivote(new List<int>() {1,2,3,4,6,7,8,9 }, validRules);
			stopWhat.Stop();
			System.Console.WriteLine(stopWhat.ElapsedMilliseconds + " ms.");
			Assert.That(result.Count, Is.EqualTo(1088));
			Assert.That(result.Contains("1818181"));
			Assert.That(result.Contains("1818183"));
			Assert.That(result.Contains("2929272"));


		}

	}
}
