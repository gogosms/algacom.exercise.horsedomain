﻿using System.Collections.Generic;

namespace Algacom.Exercise.Horse
{

	public class RulesHorse 
	{

		Dictionary<int, List<char>> _ValidRules = new Dictionary<int, List<char>>();

		public Dictionary<int, List<char>> GetRules()
		{
			_ValidRules.Add(0, new List<char>() { '4', '6' });
			_ValidRules.Add(1, new List<char>() { '8', '6' });
			_ValidRules.Add(2, new List<char>() { '7', '9' });
			_ValidRules.Add(3, new List<char>() { '8', '4' });
			_ValidRules.Add(4, new List<char>() { '9', '3', '0' });
			_ValidRules.Add(6, new List<char>() { '7', '1', '0' });
			_ValidRules.Add(7, new List<char>() { '2', '6' });
			_ValidRules.Add(8, new List<char>() { '1', '3' });
			_ValidRules.Add(9, new List<char>() { '2', '4' });

			return _ValidRules;
		}
	}

}
