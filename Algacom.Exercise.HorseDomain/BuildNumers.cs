﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Algacom.Exercise.Horse
{

	public class BuildNumers
	{
		private readonly int Max_Length = 6;

		public List<string> GetValidNumbersByPivote(int pivote, Dictionary<int, List<char>> rules)
		{
			var numbers = new List<string>();
			numbers.AddRange(GetNumbers(pivote, rules, pivote.ToString()));
			return numbers;

		}
		
		private List<string> GetNumbers(int newPivote, Dictionary<int, List<char>> rules, string number)
		{
			var numberValid = new List<string>();
			foreach (char item in rules[newPivote].SelectMany(b => b.ToString()))
			{

				if (number.Length < Max_Length)
				{
					int val = (int)Char.GetNumericValue(item);
					numberValid.AddRange(GetNumbers(val, rules, number + item));
				}

				if (number.Length == Max_Length)
				{
					numberValid.Add(number + item);
				}

			}
			return numberValid;
		}

		public List<string> GetValidNumbersByPivote(List<int> pivotes,
			Dictionary<int, List<char>> rules)
		{

			var numbers = new List<string>();
			foreach (var item in pivotes)
			{
				numbers.AddRange(GetValidNumbersByPivote(item, rules));
			}

			return numbers;

		}


	}

}
